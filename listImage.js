
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Image, Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight, TextInput, Modal, TouchableOpacity } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  state = {
    username: '',
    password: '',
    modalVisible: false
  }
  onValueChange = (field, value) => {
    this.setState({ [field]: value })
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Modal
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={{ marginTop: 22 }}>
              <View>
                <Text>Hello World!</Text>

                <TouchableOpacity
                style={styles.Modal}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Text style={styles.ModalText}>Hide Modal</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Button title="Test" onPress={() => {
            this.setModalVisible(true);
          }} />
          <Image onPress={() => {
            this.setModalVisible(true);
          }}
            style={{ width: 250, height: 250, borderRadius: 250 / 2 }}
            source={{ uri: 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/49517134_1993777407343087_2025443131536703488_o.jpg?_nc_cat=102&_nc_eui2=AeFKiTGgyV52QgjBZXttt1AoRA-epqLIS6j23hL4e9b-8ASYqQHZHwDFdORF4PbcQHHS991L5baSI1RNp8vre-bSuiCTEyIxPAyCzjA0q0d_Zg&_nc_ht=scontent-kut2-1.xx&oh=ed02b81d108e49a5a7ac8342f4b5e3a6&oe=5CF2A16D' }} />
        </View>
        <View>
          <View style={styles.Button} />
          <Text style={styles.instructions} >{this.state.username} - {this.state.password}</Text>
          <TextInput placeholder='username'
            style={{ width: 300, height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={value => { this.onValueChange("username", value) }} />
          <TextInput placeholder='password'
            style={{ width: 300, height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={value => { this.onValueChange("password", value) }} />
          <Button title='Login'
            onPress={() => {
              Alert.alert('Profile', 'กรรม')
            }}
            style={{ width: 300, height: 40 }} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  Modal: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  ModalText: {
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    marginBottom: 5,
    height: '800px'
  },
});
